import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MoviesDetailsModelBack, MoviesDetailsModelFront, MoviesListModel, MoviesListModelBack } from '../models/movies-model';
import { catchError, map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export class GetMoviesService {
  public apiKey: string = '8f68c315d0d1a84d75d2524437901b75';
  public movieListUrl: string = 'https://api.themoviedb.org/3/movie/popular?api_key=';
  public pageURL: number = 1;
  public totalPagesUrl: number = 1;
  public movieDetailsUrl: string = 'https://api.themoviedb.org/3/movie/';
  public favMovies: string[] = [];

  constructor(private http: HttpClient) { }

  public getMovieList(page: number): Observable<MoviesListModel[]> {
    return this.http.get<MoviesListModelBack>(this.movieListUrl + this.apiKey + '&page=' + page).pipe(
      map((res: MoviesListModelBack) => {
        // para saber en qué pag estamos
        // const page: {page: number, total_pages: number} = {
        //   page: res.page,
        //   total_pages: res.total_pages,
        // };
        // this.pageURL = page.page;
        // this.totalPagesUrl = page.total_pages;
        const results: MoviesListModel[] = res.results;
        const formattedResults: MoviesListModel[] = results.map(({ adult, backdrop_path, id, original_title, page, total_page}) => ({
          id,
          adult,
          backdrop_path,
          original_title,
          page: res.page,
          total_page: res.total_pages,
        }));
        return (formattedResults) ;
  }),
      catchError(err => {
        throw new Error(err.message);
      })
    );
  }

  public getMovieDetails(movieID: string): Observable<MoviesDetailsModelFront> {
    return this.http.get<MoviesDetailsModelBack>(this.movieDetailsUrl + movieID + '?api_key=' + this.apiKey).pipe(
      map((res: MoviesDetailsModelBack) => {
        console.log('respuesta api de detalles de la peli...', res);
        const results: MoviesDetailsModelFront = res;
        return (results) ;
  }),
      catchError(err => {
        throw new Error(err.message);
      })
    );
  }

  public addfav(idFavMovie: string): void{
    let match: boolean = false;
    this.favMovies.forEach(element => {
      if ( element === idFavMovie ){
        match = true;
      }
    });
    if (match === false){
      this.favMovies.push(idFavMovie);
    }
    console.log('Favorite Movies: ', this.favMovies);
  }

}
