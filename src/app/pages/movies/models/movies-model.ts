export interface MoviesListModel{
  adult: boolean,
  backdrop_path: string,
  id: number,
  original_title: string,
  page: number,
  total_page: number,
}

export interface MoviesListModelBack{
  page: number,
  results: [],
  total_pages: number,
  total_results: number,
}

export interface MoviesDetailsModelFront{
  adult: boolean,
  backdrop_path: string,
  budget: number,
  genres: [{id: number, name: string}],
  homepage: string,
  id: number,
  imdb_id: string,
  original_language: string,
  original_title: string,
  overview: string,
  popularity: number
  poster_path: string,
  production_companies:[{name:string}],
  production_countries: [{name:string}],
  release_date: string,
  revenue: number,
  runtime: number,
  spoken_languages: [{name:string}],
  status: string,
  tagline: string,
  title: string,
  video: boolean,
  vote_average: number,
  vote_count: number,
}

export interface MoviesDetailsModelBack{
  adult: boolean,
  backdrop_path: string,
  budget: number,
  genres: [{id: number, name: string}],
  homepage: string,
  id: number,
  imdb_id: string,
  original_language: string,
  original_title: string,
  overview: string,
  popularity: number
  poster_path: string,
  production_companies:[{name:string}],
  production_countries: [{name:string}],
  release_date: string,
  revenue: number,
  runtime: number,
  spoken_languages: [{name:string}],
  status: string,
  tagline: string,
  title: string,
  video: boolean,
  vote_average: number,
  vote_count: number,
}
