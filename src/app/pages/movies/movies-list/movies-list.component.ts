import { MoviesListModel } from './../models/movies-model';
import { GetMoviesService } from './../services/get-movies.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.scss']
})
export class MoviesListComponent implements OnInit {
  public movieList: MoviesListModel[] = [];
  public searchMovie: string = '';
  public filterMovie: string = '';
  public pageUrl: number = 1;
  public totalPageUrl: number = 1;
  public pagetest: number = 0;
  public favMovies: string[] = [];

  // para modal
  public closeModal: string = '';

  constructor(private getMoviesService: GetMoviesService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.router.navigate(['/movies'], { queryParams: { page: this.pageUrl }});
    this.route.queryParams.subscribe(params => {
      this.pagetest = params.page;
      console.log('Estamos en la page: ', this.pagetest);
    }
    );
    this.pageUrl = this.pagetest;
    this.getMovieListStart(this.pageUrl);
  }

  public getMovieListStart(pageUrl: number ): void{
    this.getMoviesService.getMovieList(pageUrl).subscribe(
      (data: MoviesListModel[]) => {
        const result: MoviesListModel[] = data;
        this.movieList = result;
        // guardamos las páginas en las q nos encontramos en la lista q se muestra en pantalla
        this.pageUrl = this.movieList[0].page;
        this.totalPageUrl = this.movieList[0].total_page;
        this.router.navigate(['/movies'], { queryParams: { page: this.pageUrl }});
        console.log(this.movieList);
      });
  }

  public getMovieListNext(pageUrl: number , totalPageUrl: number ): void{
    pageUrl = parseInt(`${pageUrl}` as string);
    if (pageUrl < totalPageUrl){
      pageUrl = 1 + pageUrl;
    }
    this.getMoviesService.getMovieList(pageUrl).subscribe(
      (data: MoviesListModel[]) => {
        const result: MoviesListModel[] = data;
        this.movieList = result;
        // guardamos las páginas en las q nos encontramos en la lista q se muestra en pantalla
        this.pageUrl = this.movieList[0].page;
        this.totalPageUrl = this.movieList[0].total_page;
        this.router.navigate(['/movies'], { queryParams: { page: this.pageUrl }});
        console.log(this.movieList);
      });
  }

  public getMovieListBefore(pageUrl: number, totalPageUrl: number ): void{
    if (pageUrl > 1){
      pageUrl = pageUrl - 1;
    }
    this.getMoviesService.getMovieList(pageUrl).subscribe(
      (data: MoviesListModel[]) => {
        const result: MoviesListModel[] = data;
        this.movieList = result;
        // guardamos las páginas en las q nos encontramos en la lista q se muestra en pantalla
        this.pageUrl = this.movieList[0].page;
        this.totalPageUrl = this.movieList[0].total_page;
        this.router.navigate(['/movies'], { queryParams: { page: this.pageUrl }});
        console.log(this.movieList);
      });
  }

  // public addToFavorite(idFav: string): void{
  //   let match: boolean = false;
  //   this.favMovies.forEach(element => {
  //     if ( element === idFav ){
  //       match = true;
  //     }
  //   });
  //   if (match === false){
  //     console.log('add to Fav.');
  //     this.favMovies.push (idFav);
  // }
  //   console.log('Favorite Movies: ', this.favMovies);
  // }

// service
  public addToFavorite(idFavMovie: string): void{
    this.getMoviesService.addfav(idFavMovie);
  }
}
