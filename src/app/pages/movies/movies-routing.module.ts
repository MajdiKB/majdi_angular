import { MoviesFavComponent } from './movies-fav/movies-fav.component';
import { MoviesDetailsModelFront } from './models/movies-model';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MoviesDetailsComponent } from './movies-details/movies-details.component';
import { MoviesListComponent } from './movies-list/movies-list.component';

const routes: Routes = [
    { path: '', component: MoviesListComponent },
    { path: 'details/:id', component: MoviesDetailsComponent },
    { path: 'favorites', component: MoviesFavComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MoviesRoutingModule { }
