import { MoviesDetailsModelFront } from './../models/movies-model';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { GetMoviesService } from '../services/get-movies.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-movies-details',
  templateUrl: './movies-details.component.html',
  styleUrls: ['./movies-details.component.scss']
})
export class MoviesDetailsComponent implements OnInit {
  public movieID: string = '';
  public movieDetails: MoviesDetailsModelFront = {} as MoviesDetailsModelFront;
  public pagetest: number = 0;

  constructor(private route: ActivatedRoute, private getMoviesService: GetMoviesService, private location: Location) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      // sin el as string me da error.
      this.movieID = params.get('id') as string;
    });

    this.getMoviesService.getMovieDetails(this.movieID).subscribe(
      (data: MoviesDetailsModelFront) => {
        this.movieDetails = data;
      }
    );
  }

  public goBack(): void {
    this.location.back();
  }
}
