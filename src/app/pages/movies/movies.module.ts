import { MoviesRoutingModule } from './movies-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoviesListComponent } from './movies-list/movies-list.component';
import { GetMoviesService } from './services/get-movies.service';
import { MoviesDetailsComponent } from './movies-details/movies-details.component';
import { SharedModule } from '../shared/shared.module';
import { MoviesFavComponent } from './movies-fav/movies-fav.component';

@NgModule({
  declarations: [
    MoviesListComponent,
    MoviesDetailsComponent,
    MoviesFavComponent,
  ],
  imports: [
    CommonModule,
    MoviesRoutingModule,
    SharedModule,
  ],
  providers: [GetMoviesService],
})
export class MoviesModule { }
