import { GetMoviesService } from '../services/get-movies.service';
import { MoviesDetailsModelFront } from '../models/movies-model';
import { Component, OnInit} from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-movies-fav',
  templateUrl: './movies-fav.component.html',
  styleUrls: ['./movies-fav.component.scss']
})
export class MoviesFavComponent implements OnInit {
  // @Input() favMovies: string[] = [];
   public favMovies: string [] = [];
  // @Input() get favMovies(): string[]

  //   {
  //     // if (this.favMovies !== null){
  //       console.log(this.favMovies2);
  //       return this.favMovies2;
  //     // }
  //   }

  // set favMovies(favMovies: string[])
  // {
  //   this.favMovies2 = favMovies;
  //   this.showFavMovies(favMovies);
  // }

  public movieDetails: MoviesDetailsModelFront[] = []  as MoviesDetailsModelFront[];
  constructor(private getMoviesService: GetMoviesService, private location: Location) { }

  ngOnInit(): void {
    this.showFavMovies();
    console.log(this.movieDetails);
  }

  // actualizar fav sin button
  // public showFavMovies(favMovies: string[]): MoviesDetailsModelFront[]{
  //   if (this.favMovies2){
  //   console.log('Fav Movies: ', favMovies);
  //   this.movieDetails = [];
  //   favMovies.forEach(movieId => {
  //     this.getMoviesService.getMovieDetails(movieId).subscribe(
  //       (data: MoviesDetailsModelFront) => {
  //         this.movieDetails.push(data);
  //       }
  //     );
  //   }
  // );
  // }
  //   console.log('final', this.movieDetails);
  //   return this.movieDetails;
  // }

  // si tenemos q actualizar fav con button mediante input
  // public showFavMovies(): void{
  //   console.log('Fav Movies: ', this.favMovies);
  //   this.movieDetails = [];
  //   this.favMovies.forEach(movieId => {
  //     this.getMoviesService.getMovieDetails(movieId).subscribe(
  //       (data: MoviesDetailsModelFront) => {
  //         this.movieDetails.push(data);
  //       }
  //     );
  //   });

  // }
  // con boton y servicio
    public showFavMovies(): void{
    this.favMovies = this.getMoviesService.favMovies;
    console.log('Fav Movies: ', this.getMoviesService.favMovies);
    this.movieDetails = [];
    this.getMoviesService.favMovies.forEach(movieId => {
      this.getMoviesService.getMovieDetails(movieId).subscribe(
        (data: MoviesDetailsModelFront) => {
          this.movieDetails.push(data);
        }
      );
    });

  }
  public goBack(): void {
    this.location.back();
  }
}
