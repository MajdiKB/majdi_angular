import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PersonsDetailsComponent } from './persons-details/persons-details.component';
import { PersonsListComponent } from './persons-list/persons-list.component';

const routes: Routes = [
  { path: '', component: PersonsListComponent },
  { path: ':id', component: PersonsDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonsRoutingModule { }
