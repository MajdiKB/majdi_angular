import { Component, OnInit } from '@angular/core';
import { PersonsListModelFront } from '../models/persons-model';
import { ActivatedRoute, Router } from '@angular/router';
import { GetPersonsService } from '../services/get-persons.service';

@Component({
  selector: 'app-persons-list',
  templateUrl: './persons-list.component.html',
  styleUrls: ['./persons-list.component.scss']
})
export class PersonsListComponent implements OnInit {
  public personsList: PersonsListModelFront[] = [];
  public pageUrl: number = 1;
  public totalPageUrl: number = 1;
  public filterName: string = "";
  public searchPerson: string = "";
  public pagetest: number = 1;
  constructor(private getPersonsService: GetPersonsService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.router.navigate(['/persons'], { queryParams: { page: this.pageUrl }});
    this.route.queryParams.subscribe(params => {
      this.pagetest = params.page;
    }
    );
    this.pageUrl = this.pagetest;
    this.getPersonListStart(this.pageUrl);
  }

  public getPersonListStart (pageUrl: number){
    this.getPersonsService.getPersonsList(pageUrl).subscribe(
      (data: PersonsListModelFront[]) => {
        const result: PersonsListModelFront[] = data;
        this.personsList = result;
        // guardamos las páginas en las q nos encontramos en la lista q se muestra en pantalla
        this.pageUrl = this.personsList[0].page;
        this.totalPageUrl = this.personsList[0].total_page;
        this.router.navigate(['/persons'], { queryParams: { page: this.pageUrl }});
      });
  }

  public getPersonListNext(pageUrl: number , totalPageUrl: number){
    pageUrl = parseInt(`${pageUrl}` as string);
    if (pageUrl < totalPageUrl){
      pageUrl = 1 + pageUrl;
    }
    this.getPersonsService.getPersonsList(pageUrl).subscribe(
      (data: PersonsListModelFront[]) => {
        const result: PersonsListModelFront[] = data;
        this.personsList = result;
        // guardamos las páginas en las q nos encontramos en la lista q se muestra en pantalla
        this.pageUrl = this.personsList[0].page;
        this.totalPageUrl = this.personsList[0].total_page;
        this.router.navigate(['/persons'], { queryParams: { page: this.pageUrl }});
        console.log(this.personsList);
      });
  }

  public getPersonListBefore(pageUrl: number, totalPageUrl: number ){
    if (pageUrl > 1){
      pageUrl = pageUrl - 1;
    }
    this.getPersonsService.getPersonsList(pageUrl).subscribe(
      (data: PersonsListModelFront[]) => {
        const result: PersonsListModelFront[] = data;
        this.personsList = result;
        // guardamos las páginas en las q nos encontramos en la lista q se muestra en pantalla
        this.pageUrl = this.personsList[0].page;
        this.totalPageUrl = this.personsList[0].total_page;
        this.router.navigate(['/persons'], { queryParams: { page: this.pageUrl }});
        console.log(this.personsList);
      });
  }
}
