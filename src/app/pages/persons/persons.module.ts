
import { PersonsRoutingModule } from './persons-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonsListComponent } from './persons-list/persons-list.component';
import { PersonsDetailsComponent } from './persons-details/persons-details.component';
import { SharedModule } from '../shared/shared.module';
import { GetPersonsService } from './services/get-persons.service';


@NgModule({
  declarations: [
    PersonsListComponent,
    PersonsDetailsComponent
  ],
  imports: [
    CommonModule,
    PersonsRoutingModule,
    SharedModule,
  ],
  providers: [GetPersonsService]
})
export class PersonsModule { }
