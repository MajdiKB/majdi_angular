import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { KnownForPerson, PersonCreditsModel, PersonCreditsModelBack, PersonDetailsModel, PersonsListModelBack, PersonsListModelFront } from '../models/persons-model';


@Injectable({
  providedIn: 'root'
})
export class GetPersonsService {
  public apiKey : string = "8f68c315d0d1a84d75d2524437901b75";
  public personsListUrl: string = "https://api.themoviedb.org/3/person/popular?api_key=";
  public personDetailsUrl: string = "https://api.themoviedb.org/3/person/"
  public pageURL : number = 1;
  public totalPagesUrl : number = 1;

  constructor(private http: HttpClient) { }

  public getPersonsList(page:number): Observable<PersonsListModelFront[]> {
    return this.http.get<PersonsListModelBack>(this.personsListUrl + this.apiKey + '&page=' + page).pipe(
      map((res: PersonsListModelBack)=>{
        console.log('servicio res back:',res)
        //para saber en qué pag estamos
        let page: {page: number, total_pages: number} = {
          page: res.page,
          total_pages: res.total_pages,
        };
        this.pageURL= page.page;
        this.totalPagesUrl = page.total_pages;
        const results: PersonsListModelFront[] = res.results;
        const formattedResults: PersonsListModelFront[] = results.map(({adult, gender, id, name, profile_path, popularity, known_for_department, known_for}) => {
          const person: PersonsListModelFront =
          {
            adult: adult,
            gender: gender,
            id: id,
            name: name,
            popularity: popularity,
            known_for_department: known_for_department,
            profile_path: profile_path,
            page: this.pageURL,
            total_page: this.totalPagesUrl,
            known_for: known_for.map(({backdrop_path, id, original_title })=>{
              const movie: KnownForPerson =
              {
                backdrop_path: backdrop_path,
                id: id,
                original_title: original_title,
              }
              return movie;
            })
            // known_for: [{backdrop_path: known_for.backdrop_path, id: known_for.id, original_title: known_for.original_title}]
          }
          return person;
        });
        console.log('servicio res front:', formattedResults);
        return formattedResults ;
  }),
      catchError(err => {
        throw new Error(err.message);
      })
    )
  }

  public getPersonDetails(personID: string): Observable<PersonDetailsModel> {
    return this.http.get<PersonDetailsModel>(this.personDetailsUrl + personID + '?api_key=' + this.apiKey).pipe(
      map((res: PersonDetailsModel)=>{
        console.log('respuesta api de detalles de la peli...',res);
        const results: PersonDetailsModel = res;
        return (results) ;
  }),
      catchError(err => {
        throw new Error(err.message);
      })
    )
  }

  public getPersonCredits(personID: string): Observable<PersonCreditsModel[]> {
    return this.http.get<PersonCreditsModelBack>(this.personDetailsUrl + personID + '/movie_credits?api_key=' + this.apiKey).pipe(
      map((res: PersonCreditsModelBack)=>{
        console.log('respuesta api de detalles de los creditos...',res);
        const results: PersonCreditsModel[] = res.cast;
        return (results) ;
  }),
      catchError(err => {
        throw new Error(err.message);
      })
    )
  }
}
