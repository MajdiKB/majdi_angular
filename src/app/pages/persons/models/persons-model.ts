import { FilterNameModel } from './../../shared/models/filter-name.models';
export interface PersonsListModelBack
{
  page: number;
  total_pages:number;
  results:[
    {
      adult: boolean,
      gender: number,
      id: number,
      name:string,
      known_for_department: string,
      popularity: number,
      profile_path: string,
      page: number,
      total_page: number,
      known_for: KnownForPerson[],
    }
  ];
}

export interface KnownForPerson{
  backdrop_path:string;
  id: number;
  original_title: string;
}

export interface PersonsListModelFront
{
    adult: boolean;
    gender: number;
    id: number;
    name:string;
    known_for_department: string;
    popularity: number;
    profile_path: string;
    page: number;
    total_page: number;
    known_for: KnownForPerson[];
}

export interface PersonDetailsModel{
  "adult": boolean;
  "also_known_as": [];
  "biography": string;
  "birthday": string;
  "deathday": string;
  "gender": number;
  "homepage": string;
  "id": number;
  "known_for_department": string;
  "name": string;
  "place_of_birth": string;
  "popularity": string;
  "profile_path": string;
}

export interface PersonCreditsModel{
  id: number;
  original_title: string;
  backdrop_path: string;
  poster_path: string;
  overview: string;
}

export interface PersonCreditsModelBack{
  cast: PersonCreditsModel[];
}