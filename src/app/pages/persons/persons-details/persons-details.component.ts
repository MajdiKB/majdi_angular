import { GetPersonsService } from './../services/get-persons.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { PersonCreditsModel, PersonDetailsModel } from '../models/persons-model';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-persons-details',
  templateUrl: './persons-details.component.html',
  styleUrls: ['./persons-details.component.scss']
})
export class PersonsDetailsComponent implements OnInit {
  public personID: string = '';
  public personDetails: PersonDetailsModel = {} as PersonDetailsModel;
  public personCredits: any;
  constructor(private route: ActivatedRoute, private getPersonsService: GetPersonsService, private location: Location) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      // sin el as string me da error.
      this.personID = params.get('id') as string ;
    });

    const commonObs = forkJoin([
      this.getPersonsService.getPersonDetails(this.personID),
      this.getPersonsService.getPersonCredits(this.personID),
    ]);

    commonObs.subscribe(listData => {
      this.personDetails = (listData[0] as PersonDetailsModel);
      this.personCredits = (listData[1] as PersonCreditsModel[]);
    });

    // sin el forkJoin  --> llamadas por separado a la API

    // this.getPersonsService.getPersonDetails(this.personID).subscribe(
    //   (data: PersonDetailsModel) => {
    //     this.personDetails = data;
    //   }
    // );
    // this.getPersonsService.getPersonCredits(this.personID).subscribe(
    //   (data: PersonCreditsModel[]) => {
    //     this.personCredits = data;
    //     console.log('creditos en componente detalles persona:', this.personCredits)
    //   }
    // );
  }

  public goBack(): void {
    this.location.back();
  }
}
