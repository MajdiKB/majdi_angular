import { PersonsListModelFront } from './../../persons/models/persons-model';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterName'
})
export class FilterNamePipe implements PipeTransform {

  transform(list: any[], personFilterInput: string = '', key: string): any[] {

    const filteredList: any[] = list.filter((el: any) => {
      // el include devuelve un booleano, q si encuentra algo parecido lo muestra
      const match: boolean = el[key].includes(personFilterInput);
      return match;
    });
    return filteredList;
  }
}
