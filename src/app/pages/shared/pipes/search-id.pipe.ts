import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchId'
})
export class SearchIdPipe implements PipeTransform {

  transform(list: any[], personSearchInput: string = ''): any[] {

    const filteredList: any[] = list.filter((el: any) => {
      // el include devuelve un booleano, q si encuentra algo parecido lo muestra
      const match: boolean = el.id.toString().includes(personSearchInput);
      return match;
    });

    return filteredList;
  }
}
