import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FilterNamePipe } from './pipes/filter-name.pipe';
import { SearchIdPipe } from './pipes/search-id.pipe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSliderModule } from '@angular/material/slider';

@NgModule({
  declarations: [
    FilterNamePipe,
    SearchIdPipe,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
    MatSliderModule,
  ],
  exports: [
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    FilterNamePipe,
    SearchIdPipe,
    NgbModule,
    MatSliderModule,
  ]
})
export class SharedModule { }
