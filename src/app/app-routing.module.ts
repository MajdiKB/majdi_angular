import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: `movies`, loadChildren: () =>
      import('./pages/movies/movies.module').then(m => m.MoviesModule)
  },
  {
    path: `persons`, loadChildren: () =>
      import('./pages/persons/persons.module').then(m => m.PersonsModule)
  },
  {
    path: `home`, loadChildren: () =>
      import('./pages/home/home.module').then(m => m.HomeModule)
  },
  { path: ``, redirectTo: `home`, pathMatch: `full` },
  { path: `**`, redirectTo: `home`, pathMatch: `full` },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
